//
//  SearchModelSpec.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 6..
//  Copyright © 2017년 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble

public struct MockNetwork_Successful : NetworkType {
    
    public func request(api: APISType, completion : @escaping (NSDictionary?, NetworkError?) -> ()) {
        let request = [["type":"City", "query":"London"] as NSDictionary] as NSArray
        let stub = ["data":["request" : request, "current_condition":[NSDictionary()] as NSArray] as NSDictionary] as NSDictionary
        completion( stub, nil )
    }
}

public struct MockNetwork_NoData : NetworkType {
    
    public func request(api: APISType, completion : @escaping (NSDictionary?, NetworkError?) -> ()) {
        let stub = ["msg":"no data", "code" : "101"] as NSDictionary
        completion( stub, nil )
    }
}

public struct MockNetwork_Failure : NetworkType {
    
    public func request(api: APISType, completion : @escaping (NSDictionary?, NetworkError?) -> ()) {
        completion( nil, nil )
    }
}

final public class MockStorage : StorageType {
    public init(with storage: StorageRepresentationType? ) {
        persistantStorage = storage?.datasource
    }
    
    public var persistantStorage : Fetchable?
    
    public func fetch<T : DataModelType>(query: Query, completion: @escaping (Array<T>?, StorageFetchError?) -> Void) {
        switch query {
        case .search:
            do {
                try persistantStorage?.fetchSuccesfulQuery(completion: { (datas) in
                    let fatched = datas.map{ datas in datas.map{ data in T( with : data ) } }
                    completion( fatched, nil )
                })
            } catch {
                completion( nil, StorageFetchError.unknown )
            }
        }
    }
    
    public func fetchSuccessfulQuery(completion: @escaping (Array<SingleQuery>?, StorageFetchError?) -> Void) {
        self.fetch(query: .search) { (datas, error) in
            completion( datas, error )
        }
    }
    
    public func insertSuccessfulQuery(query: String) {
        do {
            try persistantStorage?.writeSuccesfulQuery(query: query)
        } catch {
            assert(false, "developer should know error")
            // log the error developer should know
        }
    }
}

final public class MockStorage_Failure : StorageType {
    public init(with storage: StorageRepresentationType? ) {
        persistantStorage = storage?.datasource
    }
    
    public var persistantStorage : Fetchable?
    
    public func fetch<T : DataModelType>(query: Query, completion: @escaping (Array<T>?, StorageFetchError?) -> Void) {
        switch query {
        case .search:
            do {
                try persistantStorage?.fetchSuccesfulQuery(completion: { (datas) in
                    completion( nil, nil )
                })
            } catch {
                completion( nil, StorageFetchError.unknown )
            }
        }
    }
    
    public func fetchSuccessfulQuery(completion: @escaping (Array<SingleQuery>?, StorageFetchError?) -> Void) {
        var value : Array<SingleQuery>?
        value = nil
        completion( value, nil )
    }
    
    public func insertSuccessfulQuery(query: String) {
        do {
            try persistantStorage?.writeSuccesfulQuery(query: query)
        } catch {
            assert(false, "developer should know error")
            // log the error developer should know
        }
    }
}

class SearchModelSpec : QuickSpec {
    override func spec() {
        
        var successmodel : SearchModel?
        var nodatamodel : SearchModel?
        var failuredatamoddel : SearchModel?
        describe("Search Model Tests") {
            
            beforeEach {
                successmodel = SearchModel(storage: MockStorage(with: MockStorageDataSource.memorycoredata), network: MockNetwork_Successful())
                nodatamodel = SearchModel(storage: MockStorage(with: MockStorageDataSource.memorycoredata), network: MockNetwork_NoData())
                failuredatamoddel = SearchModel(storage: MockStorage_Failure(with: MockStorageDataSource.memorycoredata), network: MockNetwork_Failure())
            }
            it( "Search request in search model with proper data test" ) {
                var result : Array<WEATHER>? = nil
                var queries = Array<SingleQuery>()
                successmodel?.outputs.queryResultHandler = { array in
                    queries = array
                }
                successmodel?.outputs.searchResultHandler = { array in
                    result = array
                }
                successmodel?.inputs.searchRequestReceived(query: "London")
                successmodel?.inputs.fetchSuccessfulQuery()
                
                expect( result ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Search request in search model result should not be nil but [\(result)]")
                
                expect( successmodel?.outputs.requestWeatherDataPair.request.query ).toEventually(equal( "London"), timeout: 1.0, pollInterval: 0.0, description: "Last successful request query should be London but [\(successmodel?.outputs.requestWeatherDataPair.request.query)]")
                
                expect( queries.count ).toEventually(equal(1), timeout: 1.0, pollInterval: 0.0, description: "Saved successful query count should return 1 but [\(queries.count)]")
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    let pair = successmodel?.outputs.requestWeatherDataPair
                    expect( pair?.request ).toNot(beNil(), description: "Load data from server request should not be nil")
                    expect( pair?.weather ).toNot(beNil(), description: "Load waether data from server request should not be  nil")
                    done()
                })
                
            }
            
            it( "Search request in search model with empty query" ) {
                var result : Array<WEATHER>? = nil
                successmodel?.outputs.searchResultHandler = { array in
                    result = array
                }
                successmodel?.inputs.searchRequestReceived(query: "")
                
                expect( result ).toEventually(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Search request in search model result should be nil" )
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    let pair = successmodel?.outputs.requestWeatherDataPair
                    expect( pair?.request ).toNot(beNil(), description: "Load data from server request should not be nil")
                    expect( pair?.weather ).toNot(beNil(), description: "Load waether data from server request should not be  nil")
                    done()
                })
            }
//
            it( "Search request in search model with nodata test" ) {
                var nodataMessage : String?
                nodatamodel?.outputs.noSearchResultHandler = { msg in
                    nodataMessage = msg
                }
                nodatamodel?.inputs.searchRequestReceived(query: "London")
                
                expect( nodataMessage ).toEventually(equal("no data"), timeout: 1.0, pollInterval: 0.0, description: "Search request in search model should not be nil but [\(nodataMessage)]")
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    let pair = successmodel?.outputs.requestWeatherDataPair
                    expect( pair?.request ).toNot(beNil(), description: "Load data from server request should not be nil")
                    expect( pair?.weather ).toNot(beNil(), description: "Load waether data from server request should not be  nil")
                    done()
                })
            }
            
            it( "Search request in search model with failure test" ) {
                var queries : Array<SingleQuery>?
                var result : Array<WEATHER>?
                failuredatamoddel?.outputs.queryResultHandler = { array in
                    queries = array
                }
                failuredatamoddel?.outputs.searchResultHandler = { array in
                    result = array
                }
                failuredatamoddel?.inputs.fetchSuccessfulQuery()

                failuredatamoddel?.inputs.searchRequestReceived(query: "failure")
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    expect( queries?.count ).to(equal(0), description: "Load successful queries from core data in failure test should be 0")
                    expect( result?.count ).to(equal(0), description: "Load weather data from server in failure test should be 0")
                    done()
                })
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    let pair = successmodel?.outputs.requestWeatherDataPair
                    expect( pair?.request ).toNot(beNil(), description: "Load data from server request should not be nil")
                    expect( pair?.weather ).toNot(beNil(), description: "Load waether data from server request should not be  nil")
                    done()
                })
                
            }
            
            it( "Full automated initailize" ) {
                let searchModel = SearchModel()
                expect( searchModel ).toNot(beNil(), description: "Search model after init should not be nil")
                
            }
        }
    }
}

