//
//  NetworkSpec.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 5..
//  Copyright © 2017년 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble

enum MockAPIs : APISType {
    case londonsearch
    case emptydata
    case wrongurl
    case nilrequest
    case unparsable
    
    internal var request: URLRequest? {
        switch self {
        case .londonsearch:
            let apikey = Bundle.main.infoDictionary!["ApiKey"] as! String
            var parameters = Dictionary<String, String>()
            parameters["q"] = "london"
            parameters["fx"] = "yes"
            parameters["format"] = "json"
            parameters["key"] = apikey
            let base = Bundle.main.infoDictionary?["BaseUrl"] as? String
            let url = (base! + "weather.ashx" + "?" + parameters.query).url!
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
            
        case .emptydata:
            let apikey = Bundle.main.infoDictionary!["ApiKey"] as! String
            var parameters = Dictionary<String, String>()
            parameters["q"] = "asdfas"
            parameters["fx"] = "yes"
            parameters["format"] = "json"
            parameters["key"] = apikey
            let base = Bundle.main.infoDictionary?["BaseUrl"] as? String
            let url = (base! + "weather.ashx" + "?" + parameters.query).url!
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
            
        case .wrongurl:
            guard let url = ("").url else {
                return nil
            }
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
            
        case .unparsable:
            guard let url = ("https://www.google.com").url else {
                return nil
            }
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
            
        case .nilrequest:
            return nil
        }
    }
}

class NetworkSpec : QuickSpec {
    override func spec() {
        let network = Network()
        describe("Network test") {
            
            it( "Single result network request success case with query : London" ) {
                var dictionary : NSDictionary?
                var networkError : NetworkError?
                network.request(api: MockAPIs.londonsearch) { (dic, error) in
                    dictionary = dic
                    networkError = error
                }
                
                expect( dictionary ).toEventuallyNot(beNil(), timeout: 4.0, pollInterval: 0.0, description: "Single result network request success case : result should be not nil, but it is [\(dictionary)]")
                expect( networkError ).toEventually(beNil(), timeout: 4.0, pollInterval: 0.0, description: "Single result network request success case : networkError should be not nil, but it is [\(networkError)]")
            }
            
            it( "Single result network request failure case : Wrong URL" ) {
                var error : NetworkError?
                network.request(api: MockAPIs.wrongurl) { (dic, e) in
                    error = e
                }
                
                expect( error ).toEventually(equal(NetworkError.wrongurl), timeout: 2.0, pollInterval: 0.0, description: "Single result with wrong url network request failure case : error should be wrongurl error, but it is [\(error)]")
            }

            it( "Single result network request failure case : nil request" ) {
                var error : NetworkError?
                network.request(api: MockAPIs.nilrequest) { (dic, e) in
                    error = e
                }
                
                expect( error ).toEventually(equal(NetworkError.wrongurl), timeout: 2.0, pollInterval: 0.0, description: "Single result with nil request failure case : error should be wrongurl error, but it is [\(error)]")
            }
            
//            it( "Single result network request failure case : unparsable data from server" ) {
//                var error : NetworkError?
//                var dictionary : NSDictionary?
//                network.request(api: MockAPIs.unparsable) { (dic, e) in
//                    error = e
//                }
//                
//                expect( error ).toEventually(equal(NetworkError.unknown), timeout: 2.0, pollInterval: 0.0, description: "Single result with nil request failure case : error should be wrongurl error, but it is [\(error)]")
//            }

            it( "Single result network request failure case : empty data" ) {
                var dictionary : NSDictionary?
                network.request(api: MockAPIs.emptydata) { (dic, error) in
                    dictionary = dic
                }
                expect( dictionary?.object(forKey: "msg") as? String ).toEventually(equal("Unable to find any matching weather location to the query submitted!"), timeout: 4.0, pollInterval: 0.0, description: "Single result with wrong query : error should be nodata error, but it is [\(dictionary?.object(forKey: "msg") as? String)]")
            }
            
            it( "Single result network request failure case : empty data" ) {
                var dictionary : NSDictionary?
                network.request(api: MockAPIs.emptydata) { (dic, error) in
                    dictionary = dic
                }
                expect( dictionary?.object(forKey: "msg") as? String ).toEventually(equal("Unable to find any matching weather location to the query submitted!"), timeout: 4.0, pollInterval: 0.0, description: "Single result with wrong query : error should be nodata error, but it is [\(dictionary?.object(forKey: "msg") as? String)]")
            }
        }
    }
}
