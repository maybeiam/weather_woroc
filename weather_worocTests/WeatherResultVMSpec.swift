//
//  WeatherResultVMSpec.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble

class MockWeatherResultModel: WeatherResultModelInputs, WeatherResultModelOutputs, WeatherResultModelInOuts {
    
    unowned internal var inputs: WeatherResultModelInputs { return self }
    
    internal func reloadDataFromServer() {
        self.weatherRequestDidFinishedLoadHandler?()
    }
    
    unowned internal var outputs: WeatherResultModelOutputs { return self }
    internal var weather: WEATHER? {
        var weather = WEATHER( with : NSDictionary() )
        weather.humidity = "93"
        let format = DateFormatter()
        format.dateFormat = "mm:ss"
        weather.observation_time = format.string(from: Date())
        return weather
    }
    internal var weatherRequestDidFinishedLoadHandler: (() -> Void)?
}

class MockWeatherResultModel_EmptyWeather : WeatherResultModelInputs, WeatherResultModelOutputs, WeatherResultModelInOuts {
    
    unowned internal var inputs: WeatherResultModelInputs { return self }
    
    internal func reloadDataFromServer() {
        self.weatherRequestDidFinishedLoadHandler?()
    }
    
    unowned internal var outputs: WeatherResultModelOutputs { return self }
    internal var weather: WEATHER? {
        return nil
    }
    internal var weatherRequestDidFinishedLoadHandler: (() -> Void)?
}


class WeatherResultVMSpec: QuickSpec {
    
    override func spec() {
        
        var viewmodel : WeatherResultVM?
        var model : WeatherResultModelInOuts?
        describe("Weather Result View Model Tests") {
            
        
            it( "reload data test" ) {
                model = MockWeatherResultModel()
                viewmodel = WeatherResultVM(with: model!)
                var weather : WeatherResultCellVMInOuts?
                var date : String?
                viewmodel?.outputs.viewNeedsUpdateHandler = { () -> Void in
                    weather = viewmodel?.outputs.weather
                    date = weather?.outputs.observationTime
                }
                var finished = false
                viewmodel?.outputs.refreshFinishedHandler = { () -> Void in
                    finished = true
                }
                viewmodel?.initialLoad()
                
                waitUntil(timeout: 6.0, action: { (done) in
                    sleep(1)
                    expect( date ).toNot(beNil(), description: "both date and new date should not be nil")
                    viewmodel?.inputs.reloadData()
                    sleep(3)
                    expect( date ).toNot(beNil(), description: "both date and new date should not be nil")
                    done()
                })
                
                expect( finished ).toEventually(equal(true), timeout: 1.0, pollInterval: 0.0, description: "finished value should be true")
                
            }
            
            it( "data test with empty weather" ) {
                model = MockWeatherResultModel_EmptyWeather()
                viewmodel = WeatherResultVM(with: model!)
                var weather : WeatherResultCellVMInOuts?
                viewmodel?.outputs.viewNeedsUpdateHandler = { () -> Void in
                    weather = viewmodel?.outputs.weather
                }
                viewmodel?.initialLoad()
                viewmodel?.reloadData()
                expect( weather ).toEventually(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Request with empty weather should be nil")
                
            }
        }
    }
    
}
