//
//  SearchCellVMSpec.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble

class SearchCellVMSpec : QuickSpec {
    override func spec() {
        
        
        describe("SearchCellViewModel Tests") {
            var viewmodels : Array<SearchCellVM>?
            var datasource : Array<UILabel>?
            beforeEach {
                
                viewmodels = [SearchCellVM(with: "1"), SearchCellVM(with: "2"), SearchCellVM(with: "3")]
                datasource = [UILabel(), UILabel(), UILabel()]
            }
            it( "SearchCellViewModel data bind test" ) {
                for i in 0 ..<  datasource!.count {
                    let label = datasource![i]
                    let vm = viewmodels![i]
                    label.text = vm.outputs.query
                }
                
                
                expect( datasource![2].text ).toEventually(equal("3"), timeout: 1.0, pollInterval: 0.0, description: "Binded label text should 3" )
            }
        }
    }
}
