//
//  SearchVMSpec.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble

class MockSearchModel: SearchModelInputs, SearchModelOutputs, SearchModelInOuts {
    
    var queries = Array<String>()
    unowned internal var inputs: SearchModelInputs { return self }
    internal func searchRequestReceived(query: String) {
        if query.characters.count <= 0 {
            return
        }
        queries.append(query)
        let stub = "{\"data\":{\"request\":[{\"type\":\"City\",\"query\":\"London\"}],\"current_condition\":[{\"observation_time\":\"04:32 AM\",\"temp_C\":\"-1\",\"temp_F\":\"30\",\"weatherCode\":\"143\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png\"}],\"weatherDesc\":[{\"value\":\"Mist\"}],\"windspeedMiles\":\"11\",\"windspeedKmph\":\"17\",\"winddirDegree\":\"90\",\"winddir16Point\":\"E\",\"precipMM\":\"0.0\",\"humidity\":\"93\",\"visibility\":\"3\",\"pressure\":\"1012\",\"cloudcover\":\"0\"}],\"weather\":[{\"date\":\"2017-01-27\",\"tempMaxC\":\"7\",\"tempMaxF\":\"45\",\"tempMinC\":\"-1\",\"tempMinF\":\"31\",\"windspeedMiles\":\"11\",\"windspeedKmph\":\"19\",\"winddirection\":\"SSE\",\"winddir16Point\":\"SSE\",\"winddirDegree\":\"162\",\"weatherCode\":\"116\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png\"}],\"weatherDesc\":[{\"value\":\"Partly cloudy\"}],\"precipMM\":\"0.1\"}]}}".data(using: .utf8)
        let jsonstub = try! JSONSerialization.jsonObject(with: stub!) as! NSDictionary
        let weathers = WEATHER.array(from: jsonstub)
        fetchSuccessfulQuery()
        searchResultHandler?(weathers)
    }
    
    internal func fetchSuccessfulQuery() {
        queryResultHandler?( queries.map{ string -> SingleQuery in
            var data = SingleQuery( with : NSDictionary() )
            data.query = string
            return data
        } )
    }
    
    unowned internal var outputs : SearchModelOutputs { return self }
    
    internal var noSearchResultHandler: ((String) -> Void)?
    internal var searchResultHandler: ((Array<WEATHER>) -> Void)?
    internal var queryResultHandler: ((Array<SingleQuery>) -> Void)?
    internal var requestWeatherDataPair: (request: REQUEST, weather: WEATHER) {
        return ( REQUEST( with : NSDictionary()), weather : WEATHER(with : NSDictionary()) )
    }
}

class MockSearchModel_Failure : SearchModelInputs, SearchModelOutputs, SearchModelInOuts {
    
    var queries = Array<String>()
    unowned internal var inputs: SearchModelInputs { return self }
    internal func searchRequestReceived(query: String) {
        noSearchResultHandler?("NoData")
    }
    
    internal func fetchSuccessfulQuery() {
        
    }
    
    unowned internal var outputs : SearchModelOutputs { return self }
    
    internal var noSearchResultHandler: ((String) -> Void)?
    internal var searchResultHandler: ((Array<WEATHER>) -> Void)?
    internal var queryResultHandler: ((Array<SingleQuery>) -> Void)?
    internal var requestWeatherDataPair: (request: REQUEST, weather: WEATHER) {
        return ( REQUEST( with : NSDictionary()), weather : WEATHER(with : NSDictionary()) )
    }
}

class MockWeatherResultVM : WeatherResultVMInOuts, WeatherResultVMOutputs, WeatherResultVMInputs {
    
    unowned internal var inputs: WeatherResultVMInputs { return self }
    
    internal func reloadData() {
        model.inputs.reloadDataFromServer()
    }
    
    internal func initialLoad() {
        guard let newValue = model.outputs.weather else {
            return
        }
        _weather = WeatherResultCellsVM(with: newValue)
    }
    
    unowned internal var outputs: WeatherResultVMOutputs { return self }
    
    internal var weather: WeatherResultCellVMInOuts? {
        return _weather
    }
    internal var viewNeedsUpdateHandler: (() -> Void)?
    internal var refreshFinishedHandler: (() -> Void)?
    fileprivate var _isloading: Bool = false
    
    
    func bind() {
        model.outputs.weatherRequestDidFinishedLoadHandler = { [unowned self] () -> Void in
            guard let weather = self.model.outputs.weather else {
                return
            }
            self._weather = WeatherResultCellsVM(with: weather)
            
            DispatchQueue.main.async {
                self.viewNeedsUpdateHandler?()
            }
        }
        model.outputs.weatherRequestDidFinishedLoadHandler = { [unowned self] () -> Void in
            DispatchQueue.main.async {
                self.refreshFinishedHandler?()
            }
        }
    }
    
    fileprivate let model : WeatherResultModelInOuts
    fileprivate var _weather : WeatherResultCellVMInOuts?
    
    required init( with model : WeatherResultModelInOuts ) {
        self.model = model
        bind()
    }
}



class SearchVMSpec: QuickSpec {
    
    override func spec() {
        
        
        describe("SearchViewModel Tests") {
            var viewmodel : SearchVM?
            var model : SearchModelInOuts?
            
            afterEach {
                model = nil
            }
            it( "SearchViewModel search request test" ) {
                model = MockSearchModel()
                viewmodel = SearchVM(with: model!)
                var weathers : WeatherResultVMInOuts?
                var cellvms : Array<SearchCellVMInouts>?
                
                viewmodel!.outputs.viewNeedsUpdate = { () -> Void in
                    
                    cellvms = viewmodel!.outputs.suggestionListCellVM
                }
                viewmodel!.outputs.searchRequestResult = { (viewmodel) -> Void in
                    weathers = viewmodel
                }
                viewmodel!.inputs.searchButtonPressed(query: "London")
                
                expect( weathers ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Search request test with london and returned weather value, should not be nil" )
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    expect( cellvms!.count ).to(equal(1), description: "Result of query london count should be 1" )
                    done()
                })
            }
            
            it( "SearchViewModel search request response is no data test" ) {
                model = MockSearchModel_Failure()
                viewmodel = SearchVM(with: model!)
                var message : String?
                
                viewmodel!.outputs.searchRequestNodataResult = { (msg) -> Void in
                    message = msg
                }
                viewmodel!.inputs.searchButtonPressed(query: "London")
                
                expect( message ).toEventually(equal("NoData"), timeout: 1.0, pollInterval: 0.0, description: "Search request test no data response message should be NoData" )
            }
            
            it( "SearchViewModel stored query selection test" ) {
                model = MockSearchModel()
                viewmodel = SearchVM(with: model!)
                var cellvms : Array<SearchCellVMInouts>?
                viewmodel!.outputs.viewNeedsUpdate = { () -> Void in
                    cellvms = viewmodel!.outputs.suggestionListCellVM
                }
                
                viewmodel!.inputs.searchButtonPressed(query: "London")
                viewmodel!.inputs.searchButtonPressed(query: "LA")
                viewmodel!.inputs.didSelectSuggestion(at: 1)
                
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    expect( cellvms?.count ).to(equal(3), description: "Result of successful query count should be 3" )
                    done()
                })
            }
            
            it( "SearchViewModel leave, enter text field test" ) {
                model = MockSearchModel()
                viewmodel = SearchVM(with: model!)
                var isHidden = true
                viewmodel!.outputs.viewNeedsUpdate = { () -> Void in
                    isHidden = (viewmodel?.outputs.suggestionListTableViewIsHidden)!
                }
                
                viewmodel?.inputs.didEnterSearchTextField()
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    expect( isHidden ).to(equal(false), description: "After entering text field, ishidden value should be false" )
                    done()
                })
                viewmodel!.inputs.didLeaveSearchTextField()
                waitUntil(timeout: 3.0, action: { (done) in
                    sleep(2)
                    expect( isHidden ).to(equal(true), description: "After leaving text field, ishidden value should be true" )
                    done()
                })
            }
//
//            it( "SearchViewModel tableview hidden property test" ) {
//                let isHidden = MutableProperty( true )
//                isHidden <~ viewmodel!.outputs.rac_SuggestionListTableViewIsHidden
//                viewmodel!.inputs.didEnterSearchTextField()
//                
//                expect( isHidden.value ).toEventually(equal(false), timeout: 1.0, pollInterval: 0.0, description: "isHidden value should be false but [\(isHidden.value)]")
//                
//                viewmodel!.inputs.didLeaveSearchTextField()
//                //
//                waitUntil(timeout: 3.0, action: { (done) in
//                    sleep(2)
//                    expect( isHidden.value ).to(equal(true), description: "isHidden value should be true but [\(isHidden.value)]")
//                    done()
//                })
//            }
//            
//            it( "SearchViewModel search request response test" ) {
//                var vm : WeatherResultVMInOuts?
//                viewmodel!.outputs.searchRequestReceivedDataSignal.observeValues { value in
//                    vm = value
//                }
//                var msg = ""
//                viewmodel!.outputs.searchRequestReceivedNoDataSignal.observeValues { value in
//                    msg = value
//                }
//                viewmodel!.inputs.didEnterSearchTextField()
//                viewmodel!.inputs.searchButtonPressed(query: "London")
//                viewmodel!.inputs.searchButtonPressed(query: "LA")
//                viewmodel!.inputs.searchButtonPressed(query: "wrong")
//                expect( vm ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "response with two request should not be nil")
//                expect( msg ).toEventually(equal("wrong request"), timeout: 1.0, pollInterval: 0.0, description: "response with false request should be \"wrong request\" but [\(msg)]")
//            }
        }
    }
}
