//
//  SINGLEQUERYSpec.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 6..
//  Copyright © 2017년 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble
import CoreData

class MockSingleQueryObject : NSManagedObject {
    var type : String?
    var query : String?
}

class SINGLEQUERYSpec : QuickSpec {
    override func spec() {
        
        let stub = "{\"query\":\"London\"}".data(using: .utf8)
        
        
        describe("SINGLE QUERY parsing test") {
            var jsonstub : NSDictionary?
            
            beforeEach {
                jsonstub = try! JSONSerialization.jsonObject(with: stub!) as! NSDictionary
            }
            
            it( "Parse from stub to single query" ) {
                let query = SingleQuery( with: jsonstub!)
                expect( query ).toNot(beNil(), description: "Parse query data from dictionary : result should be not nil" )
                expect( query.query ).to(equal("London"), description: "Parse query data from dictionary should be London" )
            }
            
            it( "Parse from stub to single query failure case" ) {
                let query = SingleQuery( with: NSDictionary())
                expect( query ).toNot(beNil(), description: "Parse query data from empty dictionary : result should be not nil" )
                expect( query.query ).to(equal(""), description: "Parse query data from empty dictionary should be empty string" )
            }
            
            it( "Parse from request object") {
                let object = MockSingleQueryObject()
                object.query = nil//"Lerndern"
                let request = SingleQuery( with : object )
                expect( request.query ).to(equal(""), description: "Parse query data from Key value codable object stores nil, data should be empty string" )
            }
            
        }
    }
}
