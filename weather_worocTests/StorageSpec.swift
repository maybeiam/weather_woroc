//
//  StorageSpec.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 6..
//  Copyright © 2017년 Jin. All rights reserved.
//

@testable import weather_woroc
import CoreData
import Quick
import Nimble

public enum MockStorageDataSource : StorageRepresentationType {
    case memorycoredata
    case insertfail
    
    public var datasource : Fetchable? {
        switch self {
        case .memorycoredata:
            return MockCoreData()
        case .insertfail:
            return MockCoreData_Failure()
        }
    }
}

class MockCoreData : Fetchable {
    var fetched = Array<NSManagedObject>()
    
    public func fetchSuccesfulQuery( completion : @escaping (Array<NSObject>?) -> () ) throws {
        completion( fetched )
    }
    
    public func writeSuccesfulQuery(query: String) throws  {
        let querycount = fetched.filter { (managedObject) -> Bool in
            return managedObject.value(forKey: "query" ) as? String == query as String
            }.count
        if querycount > 0 {
            return
        }
        let object = MockSuccessfulQuery()
        object.date = NSDate()
        object.query = query == "" ? nil : query
        fetched.append( object )
    }
}

class MockCoreData_Failure : Fetchable {
    var fetched = Array<NSManagedObject>()
    
    public func fetchSuccesfulQuery( completion : @escaping (Array<NSObject>?) -> () ) throws {
        throw StorageFetchError.unknown
    }
    
    public func writeSuccesfulQuery(query: String) throws  {
//        assert( false )
        throw CoreDataError.persistentStorage
    }
}

class MockSuccessfulQuery : NSManagedObject {
    var query : String?
    var date : NSDate?
}

class StorageSpec : QuickSpec {
    
    override func spec() {
        context( "Storage Tests") {
            describe("CoreData Tests") {
                
                it( "Fetch stored data test") {
                    let storage = Storage( with : MockStorageDataSource.memorycoredata )
                    
                    storage.insertSuccessfulQuery( query : "1" )
                    var array : Array<SingleQuery>?
                    storage.fetchSuccessfulQuery { (result, error) in
                        array = result
                    }
                    
                    expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                    waitUntil(timeout: 3.0, action: { (done) in
                        sleep(2)
                        expect( array?.count ).to(equal(1), description: "Result of fetching successful query count should be 1")
                        done()
                    })
                }
                
                it( "insert. fetch data failure test") {
                    let storage = Storage( with : MockStorageDataSource.insertfail )
                    var fetchError : StorageFetchError?
                    storage.fetchSuccessfulQuery { (result, e) in
                        fetchError = e
                    }
                    
                    expect{ storage.insertSuccessfulQuery( query : "1" ) }.to(throwAssertion(), description : "Insert data to failure core data object")
                    
                    expect( fetchError ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Error of fail fetch should be not nil")
                    waitUntil(timeout: 3.0, action: { (done) in
                        sleep(2)
                        expect( fetchError ).to(equal(StorageFetchError.unknown), description: "Storage fetch error should be unknown")
                        done()
                    })
                }
                
                it( "Storage object loaded normally" ) {
                    let storage = Storage( with : PersistentStorageDataSource.coredata )
                    expect( storage ).toNot(beNil(), description: "Normally loaded storage should not be nil" )
                }
                
            }
        }
    }
    
}
