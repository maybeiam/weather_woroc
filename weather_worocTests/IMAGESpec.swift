//
//  IMAGESpec.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 5..
//  Copyright © 2017년 Jin. All rights reserved.
//
@testable import weather_woroc
import Quick
import Nimble

class MockIMAGEObject : NSObject {
    var url : NSURL?
    var image : UIImage?
}


class IMAGESpec : QuickSpec {
    override func spec() {
        
        let stub = "{\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png\"}]}".data(using: .utf8)
        
        
        describe("IMAGE parsing test") {
            var jsonstub : NSDictionary?
            
            beforeEach {
                jsonstub = try! JSONSerialization.jsonObject(with: stub!) as! NSDictionary
            }
            
            it( "Parse from stub to image result" ) {
                let array = jsonstub?.object(forKey: "weatherIconUrl" ) as! NSArray
                let value = array.object(at: 0) as! NSDictionary
                let image = IMAGE(with: value)
                expect( image.url ).toNot(beNil(), description: "Parse IMAGE result should be not nil, but it is [\(image.url)]")
                expect( image.url?.absoluteString ).to(equal("http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png"), description: "Parse IMAGE url from stub should be not nil, but it is [\(image.url?.absoluteString)]")
            }
            
            it( "Parse from object to image result" ) {
                let object = MockIMAGEObject()
                object.image = UIImage()
                object.url = NSURL( string : "https://www.google.com" )
                let image = IMAGE( with : object )
                expect( image.image ).to(beNil(), description: "Parse image data from Key value codable object, data should be not nil" )
                expect( image.url!.absoluteString ).to(equal("https://www.google.com"), description: "Parse request data from Key value codable object, data should be https://www.google.com" )
            }
        }
    }
}
