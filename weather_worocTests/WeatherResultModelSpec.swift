//
//  WeatherResultSpec.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble

class WeatherResultModelSpec: QuickSpec {
    
    override func spec() {
        
        var model : WeatherResultModel?
        describe("Weather Result Model Tests") {
            beforeEach {
                model = nil
            }
            
            it( "reload data test" ) {
                let request = REQUEST(with: ["type":"City", "query":"London"] as NSDictionary )
                model = WeatherResultModel(request: request, weather: WEATHER( with : NSDictionary() ), network: MockNetwork_Successful())
                var weather : WEATHER?
                model?.outputs.weatherRequestDidFinishedLoadHandler = { () -> Void in
                    weather = model?.outputs.weather
                }
                model?.inputs.reloadDataFromServer()
                expect( weather ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "weather value after reload request should not be nil but [\(weather)]")
            }
            
            it( "reload data test with error response" ) {
                let request = REQUEST(with: ["type":"City", "query":"London"] as NSDictionary )
                model = WeatherResultModel(request: request, weather: WEATHER( with : NSDictionary() ), network: MockNetwork_Failure())
                
                var weather : WEATHER?
                model?.outputs.weatherRequestDidFinishedLoadHandler = { () -> Void in
                    weather = model?.outputs.weather
                }
                model?.inputs.reloadDataFromServer()
                expect( weather?.weatherDesc ).toEventually(beNil(), timeout: 1.0, pollInterval: 0.0, description: "weather value after reload request should be nil" )
            }
            
            it( "reload data with empty request failure test" ) {
                model = WeatherResultModel( network: MockNetwork_Successful())
                var weather : WEATHER?
                model?.outputs.weatherRequestDidFinishedLoadHandler = { () -> Void in
                    weather = model?.outputs.weather
                }
                model?.inputs.reloadDataFromServer()
                expect( weather ).toEventually(beNil(), timeout: 1.0, pollInterval: 0.0, description: "weather value after reload request should be nil" )
            }
            
            it( "reload data with 0 length query failure test" ) {
                let request = REQUEST(with: ["type":"City", "query":""] as NSDictionary )
                model = WeatherResultModel(request: request, weather: WEATHER( with : NSDictionary() ), network: MockNetwork_Successful())
                var weather : WEATHER?
                model?.outputs.weatherRequestDidFinishedLoadHandler = { () -> Void in
                    weather = model?.outputs.weather
                }
                model?.inputs.reloadDataFromServer()
                expect( weather ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "weather value after reload request should not be nil but [\(weather)]")
            }
            
            it( "reload data with empty request query failure test" ) {
                let request = REQUEST(with: ["type":"City"] as NSDictionary )
                model = WeatherResultModel(request: request, weather: WEATHER( with : NSDictionary() ), network: MockNetwork_Successful())
                
                var weather : WEATHER?
                model?.outputs.weatherRequestDidFinishedLoadHandler = { () -> Void in
                    weather = model?.outputs.weather
                }
                model?.inputs.reloadDataFromServer()
                expect( weather?.weatherDesc ).toEventually(beNil(), timeout: 1.0, pollInterval: 0.0, description: "weather value after reload request should be nil" )
            }
            
            it( "initialize without parameter test") {
                model = WeatherResultModel()
                expect( model?.outputs.weather ).to(beNil(), description: "weather value after reload request should be nil" )
            }
            
            it( "initialize with request and weather test") {
                let request = REQUEST(with: ["type":"City", "query":""] as NSDictionary )
                model = WeatherResultModel(request: request, weather: WEATHER( with : NSDictionary() ))
                expect( model ).toNot(beNil(), description: "weather value after reload request should be nil" )
            }
            
            
        }
    }
    
}
