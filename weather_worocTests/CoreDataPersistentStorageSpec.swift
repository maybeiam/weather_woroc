//
//  PersistentStorageSpec.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 4..
//  Copyright © 2017년 Jin. All rights reserved.
//

@testable import weather_woroc
import CoreData
import Quick
import Nimble

class CoreDataPersistentStorageSpec : QuickSpec {
    override func spec() {
        
        context( "CoreDataStorage Tests") {
            
            describe("Core data initialize test") {
                it( "Core data memory storage initialize failure test" ) {
                    expect{ try CoreDataPersistentStorage.generateMemoryPersistentStoreCoordinator( name : "dummy" ) }.to(throwError(), description : "load persistent storage with name dummy should throw error")
                }
                
                it( "Core data memory storage initialize success test" ) {
                    expect{ try CoreDataPersistentStorage.generateMemoryPersistentStoreCoordinator( name : "weather_woroc" ) }.toNot(throwError(),description : "load persistent storage with proper name should not throw error")
                }
                
                it( "Core data sqlite storage initialize failure test" ) {
                    expect{ try CoreDataPersistentStorage.generateSqlitePersistentStoreCoordinator( name : "dummy" ) }.to(throwError(), description : "load persistent storage with name dummy should throw error")
                }
                
                it( "Core data sqlite storage initialize success test" ) {
                    expect{ try CoreDataPersistentStorage.generateSqlitePersistentStoreCoordinator( name : "weather_woroc" ) }.toNot(throwError(), description : "load persistent storage with proper name should not throw error")
                }
                
                it( "Core data memory storage initialize success test, with CoreDataPersistentStorage init" ) {
                    let storage = CoreDataPersistentStorage(persistentStorage: try! CoreDataPersistentStorage.generateMemoryPersistentStoreCoordinator(name: "weather_woroc"))
                    expect{ storage }.toNot(beNil())
                }
            }
            
            describe("Core data insert test") {
                
                it( "Core data writing query failure case") {
                    var coredata : CoreDataPersistentStorage?
                    let modelUrl = Bundle(for: CoreDataPersistentStorageSpec.self).url(forResource: "MockModel", withExtension: "momd")!
                    //                        Bundle.main.url(forResource: "MockModel", withExtension: "momd")!
                    let mom = NSManagedObjectModel(contentsOf: modelUrl)!
                    let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
                    let _ = try? psc.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
                    coredata = CoreDataPersistentStorage(persistentStorage: psc)
                    
                    expect{ try coredata?.writeSuccesfulQuery(query: "1") }.to(raiseException(), description : "insert a data to empty entitiy")
                    
                }
                
                it( "Core data managedObjectContextFailure case") {
                    var coredata : CoreDataPersistentStorage?
                    coredata = CoreDataPersistentStorage(persistentStorage: try! CoreDataPersistentStorage.generateMemoryPersistentStoreCoordinator( name : "weather_woroc") )
//
                    expect{ try coredata!.insert { (moc) in
                        
                        let object = NSEntityDescription.insertNewObject(forEntityName: "SuccessfulQuery", into: moc) as? SuccessfulQuery
                        object?.date = nil
                        object?.query = "query"
                        }
                        }.to(throwError(), description : "ManagedObjectContext save action with entity with nil attribute should throw exception")
                }
                
                it( "Core data writing query failure case with entity exist") {
                    
                    var coredata : CoreDataPersistentStorage?
                    coredata = CoreDataPersistentStorage(persistentStorage: try! CoreDataPersistentStorage.generateMemoryPersistentStoreCoordinator( name : "weather_woroc") )
                    
                    expect{ try coredata?.writeSuccesfulQuery(query: "") }.to(throwError(), description : "ManagedObjectContext save action with entity with empty query should throw exception")
                    
                }
            }
            
            describe("Core data fetch test") {
                it( "Core data fetch with wrong entity name") {
                    
                    var coredata : CoreDataPersistentStorage?
                    coredata = CoreDataPersistentStorage(persistentStorage: try! CoreDataPersistentStorage.generateMemoryPersistentStoreCoordinator( name : "weather_woroc") )
                    expect{ coredata!.fetch(entity: "SuccessfulQueri") }.to(raiseException(), description : "insert a data to wrong entitiy")
                    
                    
                }
                
                it( "Core data fetch entity, sort descriptor, ascending flag") {
                    
                    var coredata : CoreDataPersistentStorage?
                    coredata = CoreDataPersistentStorage(persistentStorage: try! CoreDataPersistentStorage.generateMemoryPersistentStoreCoordinator( name : "weather_woroc") )
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    try! coredata!.writeSuccesfulQuery(query: "2")
                    try! coredata!.writeSuccesfulQuery(query: "3")
                    try! coredata!.writeSuccesfulQuery(query: "4")
                    let array = coredata!.fetch(entity: "SuccessfulQuery", sortBy: "date", ascending: false)
                    expect( array ).toNot( beNil(), description : "fetched array should not be nil")
                    expect( array!.count ).to( equal(4), description : "fetched array should not be nil")
                    
                }
                
                
                
                
            }
            
            describe("Core data insert, delete, fetch logic test") {
                var coredata : CoreDataPersistentStorage?
                beforeEach {
                    coredata = CoreDataPersistentStorage(persistentStorage: try! CoreDataPersistentStorage.generateMemoryPersistentStoreCoordinator( name : "weather_woroc") )
                }
                
                it( "Insert data" ) {
                    var array : Array<NSObject>?
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    try! coredata!.fetchSuccesfulQuery { result in
                        array = result
                    }
                    expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                    expect( array!.count ).toEventually(equal(1), timeout: 2.0, pollInterval: 0.0, description: "Result of fetching successful query count should be 1 but [\(array!.count)]")
                }
                
                it( "Duplicate test" ) {
                    var array : Array<NSObject>?
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    
                    expect{ try coredata!.fetchSuccesfulQuery { result in
                        array = result
                        }
                    }.toNot(throwError(), description : "Fetch while duplicate test should not throw error")
                    
                    
                    expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                    expect( array!.count ).toEventually(equal(1), timeout: 2.0, pollInterval: 0.0, description: "Result of fetching successful query count should be 1 but [\(array!.count)]")
                }
                
                it( "Insert 10 datas" ) {
                    var array : Array<NSObject>?
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    try! coredata!.writeSuccesfulQuery(query: "2")
                    try! coredata!.writeSuccesfulQuery(query: "3")
                    try! coredata!.writeSuccesfulQuery(query: "4")
                    try! coredata!.writeSuccesfulQuery(query: "5")
                    try! coredata!.writeSuccesfulQuery(query: "6")
                    try! coredata!.writeSuccesfulQuery(query: "7")
                    try! coredata!.writeSuccesfulQuery(query: "8")
                    try! coredata!.writeSuccesfulQuery(query: "9")
                    try! coredata!.writeSuccesfulQuery(query: "10")
                    expect{ try coredata!.fetchSuccesfulQuery { result in
                        array = result
                        }
                        }.toNot(throwError(), description : "Fetch while inserting 10 data test should not throw error")
                    expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                    expect( array!.count ).toEventually(equal(10), timeout: 2.0, pollInterval: 0.0, description: "Result of fetching successful query count should be 10 but [\(array!.count)]")
                }
                
                it( "Insert 11 or more datas" ) {
                    var array : Array<NSObject>?
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    try! coredata!.writeSuccesfulQuery(query: "2")
                    try! coredata!.writeSuccesfulQuery(query: "3")
                    try! coredata!.writeSuccesfulQuery(query: "4")
                    try! coredata!.writeSuccesfulQuery(query: "5")
                    try! coredata!.writeSuccesfulQuery(query: "6")
                    try! coredata!.writeSuccesfulQuery(query: "7")
                    try! coredata!.writeSuccesfulQuery(query: "8")
                    try! coredata!.writeSuccesfulQuery(query: "9")
                    try! coredata!.writeSuccesfulQuery(query: "10")
                    try! coredata!.writeSuccesfulQuery(query: "11")
                    try! coredata!.writeSuccesfulQuery(query: "12")
                    try! coredata!.writeSuccesfulQuery(query: "13")
                    expect{ try coredata!.fetchSuccesfulQuery { result in
                        array = result
                        }
                        }.toNot(throwError(), description : "Fetch while inserting 10 or more datas test should not throw error")
                    expect( array ).toEventuallyNot(beNil(), timeout: 1.0, pollInterval: 0.0, description: "Result of fetching successful query should not nil but [\(array)]")
                    expect( array!.count ).toEventually(equal(10), timeout: 2.0, pollInterval: 0.0, description: "Result of fetching successful query count should be 10 but [\(array!.count)]")
                    
                }
                
                it( "Last data test" ) {
                    var array : Array<NSObject>?
                    
                    try! coredata!.writeSuccesfulQuery(query: "1")
                    try! coredata!.writeSuccesfulQuery(query: "2")
                    try! coredata!.writeSuccesfulQuery(query: "3")
                    try! coredata!.writeSuccesfulQuery(query: "4")
                    try! coredata!.writeSuccesfulQuery(query: "5")
                    try! coredata!.writeSuccesfulQuery(query: "6")
                    try! coredata!.writeSuccesfulQuery(query: "7")
                    try! coredata!.writeSuccesfulQuery(query: "8")
                    try! coredata!.writeSuccesfulQuery(query: "9")
                    try! coredata!.writeSuccesfulQuery(query: "10")
                    try! coredata!.writeSuccesfulQuery(query: "11")
                    try! coredata!.writeSuccesfulQuery(query: "12")
                    try! coredata!.writeSuccesfulQuery(query: "13")
                    
                    expect{ try coredata!.fetchSuccesfulQuery { result in
                        array = result
                        }
                        }.toNot(throwError(), description : "Fetch while last data test should not throw error")
                    waitUntil(timeout: 3.0, action: { (done) in
                        sleep(2)
                        expect( array!.first!.value(forKey: "query") as? String ).to(equal("13"), description: "Last result of fetching successful query count should be \"13\" but [\(array!.first!.value(forKey: "query") as! String)]")
                        expect( array!.last!.value(forKey: "query") as? String ).to(equal("4"), description: "Result of fetching successful query count should be \"1\" but [\(array!.last!.value(forKey: "query") as! String)]")
                        done()
                    })
                    
                }
                
            }
            
        }
        
        
    }
    
}
