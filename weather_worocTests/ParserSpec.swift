//
//  ParserSpec.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 4..
//  Copyright © 2017년 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble

class ParserSpec: QuickSpec {
    let errorparser = WeatherErrorParser()
    let jsonparser = JSONParser()
    
    override func spec() {
        context( "Parsers Tests") {
            describe("Json Parser Tests ") {
                let stub = "{\"data\":{\"request\":[{\"type\":\"City\",\"query\":\"London\"}],\"current_condition\":[{\"observation_time\":\"04:32 AM\",\"temp_C\":\"-1\",\"temp_F\":\"30\",\"weatherCode\":\"143\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png\"}],\"weatherDesc\":[{\"value\":\"Mist\"}],\"windspeedMiles\":\"11\",\"windspeedKmph\":\"17\",\"winddirDegree\":\"90\",\"winddir16Point\":\"E\",\"precipMM\":\"0.0\",\"humidity\":\"93\",\"visibility\":\"3\",\"pressure\":\"1012\",\"cloudcover\":\"0\"}],\"weather\":[{\"date\":\"2017-01-27\",\"tempMaxC\":\"7\",\"tempMaxF\":\"45\",\"tempMinC\":\"-1\",\"tempMinF\":\"31\",\"windspeedMiles\":\"11\",\"windspeedKmph\":\"19\",\"winddirection\":\"SSE\",\"winddir16Point\":\"SSE\",\"winddirDegree\":\"162\",\"weatherCode\":\"116\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png\"}],\"weatherDesc\":[{\"value\":\"Partly cloudy\"}],\"precipMM\":\"0.1\"}]}}".data(using: .utf8)
                let falseStub = "{\"data\":{\"request\":[{\"type\":\"City\",\"query\":\"London\"}],\"current_condition\":[{\"observation_time\":\"04:32 AM\",\"temp_C\":\"-1\",\"temp_F\":\"30\",\"weatherCode\":\"143\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png\"}],\"weatherDesc\":[{\"value\":\"Mist\"}],\"windspeedMiles\":\"11\",\"windspeedKmph\":\"17\",\"winddirDegree\":\"90\",\"winddir16Point\":\"E\",\"precipMM\":\"0.0\",\"humidity\":\"93\",\"visibility\":\"3\",\"pressure\":\"1012\",\"cloudcover\":\"0\"},\"weather\":[{\"date\":\"2017-01-27\",\"tempMaxC\":\"7\",\"tempMaxF\":\"45\",\"tempMinC\":\"-1\",\"tempMinF\":\"31\",\"windspeedMiles\":\"11\",\"windspeedKmph\":\"19\",\"winddirection\":\"SSE\",\"winddir16Point\":\"SSE\",\"winddirDegree\":\"162\",\"weatherCode\":\"116\",\"weatherIconUrl\":[{\"value\":\"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png\"}],\"weatherDesc\":[{\"value\":\"Partly cloudy\"}],\"precipMM\":\"0.1\"}]}}".data(using: .utf8)
                
                it( "Jsonparser with nil data test" ) {
                    let result = self.jsonparser.parse(with: nil)
                    expect( result ).to(beNil(), description: "Json parser result with nil data should be nil" )
                }
                
                it( "Jsonparser with impossible to parse data test" ) {
                    let result = self.jsonparser.parse(with: falseStub)
                    expect( result ).to(beNil(), description: "Json parser result un parsable data should be nil" )
                }
                
                it( "Jsonparser with proper data test" ) {
                    let result = self.jsonparser.parse(with: stub)
                    expect( result ).toNot(beNil(), description: "Json parser result with proper data should be not nil" )
                }
            }
            describe("Error Parser tests") {
                
                it( "WeatherParser with empty dictionary test" ) {
                    let stub : NSDictionary? = nil
                    let result = self.errorparser.parse(with: stub)
                    let msg = result?.object(forKey: "msg") as? String
                    expect( msg ).to(equal("Empty request result error"), description: "Error parser with empty dictionary should be Empty request result error but it is [\(msg)]")
                }
                
                it( "WeatherParser with no data test" ) {
                    let stub = ["nodata":"nodata"] as NSDictionary
                    let result = self.errorparser.parse(with: stub)
                    let msg = result?.object(forKey: "msg") as? String
                    expect( msg ).to(equal("Wrong format error"), description: "Error parser with empty dictionary should be Wrong format error but it is [\(msg)]")
                }
                
                it( "WeatherParser with non error data test" ) {
                    let stub = ["data":["nodata":"dummy"] as NSDictionary] as NSDictionary
                    let result = self.errorparser.parse(with: stub)
                    expect( result ).to(beNil(), description: "Error parser with dictionary data -> error should return nil")
                }
                
                it( "WeatherParser with proper dataset test" ) {
                    let stub = ["data":["error" : [["msg":"Unable to find any matching weather location to the query submitted!"] as NSDictionary] as NSArray] as NSDictionary] as NSDictionary
                    let result = self.errorparser.parse(with: stub)
                    let msg = result?.object(forKey: "msg") as? String
                    let originMessage = (((stub.object(forKey: "data") as! NSDictionary).object(forKey: "error") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "msg") as! String
                    expect( msg ).to(equal(originMessage), description: "Error parser with proper data set dictionary should be [\(originMessage)] but it is [\(msg)]")
                }
            }
        }
        
        
    }
    
}
