//
//  WeatherResultCellVMSpec.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble

class WeatherResultCellVMSpec: QuickSpec {
    
    override func spec() {
        
        var viewmodel : WeatherResultCellsVM?
        describe("Weather Result Cell View Model Tests") {
            
            
            it( "Data load verification" ) {
                var image : UIImage?
                let observationtime : String?
                let humidity : String?
                let description : String?
                var datamodel = WEATHER( with : NSDictionary() )
                datamodel.humidity = "10.0"
                datamodel.weatherDesc = "Partly Cloudy"
                datamodel.observation_time = "14 : 00"
                datamodel.weatherIcon = IMAGE(with: NSDictionary() )
                datamodel.weatherIcon?.url = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png".url?.https
                
                viewmodel = WeatherResultCellsVM(with: datamodel)
                observationtime = viewmodel!.outputs.observationTime
                humidity = viewmodel!.outputs.humidity
                description = viewmodel!.outputs.weatherDescription
                
                viewmodel!.viewNeedsUpdateImageHandler = { () -> Void in
                    image = viewmodel!.outputs.weatherIcon
                }
                
                expect( observationtime ).toEventually(equal( datamodel.observation_time ), timeout: 1.0, pollInterval: 0.0, description: "observation time value should be equal to datamodel")
                expect( humidity).toEventually(equal( datamodel.humidity ), timeout: 1.0, pollInterval: 0.0, description: "humidity value should be equal to datamodel")
                expect( description ).toEventually(equal( datamodel.weatherDesc ), timeout: 1.0, pollInterval: 0.0, description: "description value should be equal to datamodel")
                expect( image ).toEventuallyNot(beNil(), timeout: 2.0, pollInterval: 0.0, description: "weather Icon value could be downloaded")
            }
            
            it( "Empty response test" ) {
                var image : UIImage?
                var datamodel = WEATHER( with : NSDictionary() )
                datamodel.weatherIcon = IMAGE(with: NSDictionary() )
                datamodel.weatherIcon?.url = "wrongurl".url
                
                viewmodel = WeatherResultCellsVM(with: datamodel)
                
                viewmodel!.viewNeedsUpdateImageHandler = { () -> Void in
                    image = viewmodel!.outputs.weatherIcon
                }
                expect( image ).toEventually(beNil(), timeout: 2.0, pollInterval: 0.0, description: "weather Icon value with wrong request should be nil")
            }
            
            it( "Not an image response test" ) {
                var image : UIImage?
                
                var datamodel = WEATHER( with : NSDictionary() )
                datamodel.weatherIcon = IMAGE(with: NSDictionary() )
                datamodel.weatherIcon?.url = "https://www.google.com".url?.http
                
                viewmodel = WeatherResultCellsVM(with: datamodel)
                
                viewmodel!.viewNeedsUpdateImageHandler = { () -> Void in
                    image = viewmodel!.outputs.weatherIcon
                }
                expect( image ).toEventually(beNil(), timeout: 2.0, pollInterval: 0.0, description: "weather Icon value with wrong request should be nil")
            }
            
            it( "Nil url request test" ) {
                var image : UIImage?
                
                var datamodel = WEATHER( with : NSDictionary() )
                datamodel.weatherIcon = IMAGE(with: NSDictionary() )
                datamodel.weatherIcon?.url = nil
                
                viewmodel = WeatherResultCellsVM(with: datamodel)
                
                viewmodel!.viewNeedsUpdateImageHandler = { () -> Void in
                    image = viewmodel!.outputs.weatherIcon
                }
                expect( image ).toEventually(beNil(), timeout: 2.0, pollInterval: 0.0, description: "weather Icon value with wrong request should be nil")
            }
        }
    }
    
}

