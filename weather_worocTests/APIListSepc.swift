//
//  APIsTest.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 4..
//  Copyright © 2017년 Jin. All rights reserved.
//

@testable import weather_woroc
import Quick
import Nimble

class APIListSpec : QuickSpec {
    override func spec() {
        describe("APIList test") {
            
            it( "Single request with query london" ) {
                let q = "london"
                let request = APIs.search(query: q).request
                let componenets = URLComponents(url: request!.url!, resolvingAgainstBaseURL: true)
                
                let queries = componenets?.query?.components(separatedBy: "&")
                let dic = queries?.reduce(Dictionary<String,String>(), { (dic, value) -> Dictionary<String,String> in
    
                    let array = value.components(separatedBy: "=")
                    var newdictionary = dic
                    newdictionary[array[0]] = array[1]
                    return newdictionary
                })
                expect( componenets?.scheme ).to(equal("https"), description : "API request scheme should be https")
                expect( dic!["key"]! ).to(equal("vzkjnx2j5f88vyn5dhvvqkzc"), description : "API request query, api key check")
                expect( dic!["format"]! ).to(equal("json"), description : "API request query, format check")
                expect( dic!["q"]! ).to(equal(q), description : "API request query, query check")
                expect( dic!["fx"]! ).to(equal("yes"), description : "API request query, fx check")
            }
            
            it( "Single request with query empty string, should be failure" ) {
                
                let q = ""
                let request = APIs.search(query: q).request
                expect( request ).to( beNil(), description : "API request with false query should be nil")
            }
        }
    }
}

//extension Bundle {
//    override var baseurl : String {
//        
//    }
//}
