//
//  APIList.swift
//  weather
//
//  Created by 1002220 on 03/02/2017.
//  Copyright © 2017 weather. All rights reserved.
//

import Foundation

fileprivate enum URLS {
    fileprivate static var base : String? {
        return Bundle.main.infoDictionary?["BaseUrl"] as? String
    }
    fileprivate static var weather : String {
        return "weather.ashx"
    }
}

public protocol APISType {
    var request: URLRequest? { get }
}

public enum APIs : APISType {
    case search( query : String )
    
    public var request: URLRequest? {
        switch self {
        case let .search( query ):
            let apikey = Bundle.main.infoDictionary!["ApiKey"] as! String
            var parameters = Dictionary<String, String>()
            parameters["q"] = query
            parameters["fx"] = "yes"
            parameters["format"] = "json"
            parameters["key"] = apikey
            
            if query.characters.count <= 0 || URLS.base == nil || (URLS.base! + URLS.weather + "?" + parameters.query).url == nil {
                return nil
            }
            let url = (URLS.base! + URLS.weather + "?" + parameters.query).url!
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
        }
    }
}
