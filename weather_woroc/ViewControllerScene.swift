//
//  ViewControllerScene.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 5..
//  Copyright © 2017년 Jin. All rights reserved.
//
import UIKit

public enum Storyboard: String {
    case Main
    case Weather
    
    public func instantiate<VC: UIViewController>(_ viewController: VC.Type) -> VC {
        guard let vc = UIStoryboard(name: self.rawValue, bundle: Bundle.main)
            .instantiateViewController(withIdentifier: VC.storyboardIdentifier) as? VC
            else { fatalError("Couldn't instantiate \(VC.storyboardIdentifier) from \(self.rawValue)") }
        
        return vc
        
    }
}
