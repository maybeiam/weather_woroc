////
////  SearchModel.swift
////  weather_woroc
////
////  Created by Shepherd on 2017. 2. 6..
////  Copyright © 2017년 Jin. All rights reserved.
////
//
import UIKit

protocol SearchModelInputs : class {
    func searchRequestReceived( query : String )
    func fetchSuccessfulQuery()
}

protocol SearchModelOutputs : class {
    var queryResultHandler : ((Array<SingleQuery>) -> Void)? { get set }
    var noSearchResultHandler : ((String) -> Void)? { get set }
    var searchResultHandler : ((Array<WEATHER>) -> Void)?  { get set }
    var requestWeatherDataPair : (request : REQUEST, weather : WEATHER) { get }
}

protocol SearchModelInOuts : class {
    var inputs : SearchModelInputs { get }
    var outputs : SearchModelOutputs { get }
}

internal final class SearchModel: SearchModelInputs, SearchModelOutputs, SearchModelInOuts {
    
    unowned internal var inputs: SearchModelInputs { return self }
    
    internal func searchRequestReceived(query: String) {
        if query.characters.count <= 0 {
            return
        }
        network.request(api: APIs.search(query: query)) { [unowned self] (dictionary, error) in
            if error == nil {
                if let msg = dictionary?.object(forKey: "msg") as? String {
                    self.noSearchResultHandler?(msg)
                } else {
                    self.storage.insertSuccessfulQuery(query: query)
                    self.storage.fetchSuccessfulQuery(completion: { (array, error) in
                        if error == nil {
                            self._queryResult = array ?? Array<SingleQuery>()
                            self.queryResultHandler?(self._queryResult)
                        }
                    })
                    
                    let weathers = WEATHER.array(from: dictionary ?? NSDictionary())
                    if weathers.count > 0 {
                        self._lastWeather = weathers.last!
                    }
                    let request = REQUEST.array(from: dictionary ?? NSDictionary() )
                    if request.count > 0 {
                        self._lastRequest = request.last!
                    }
                    self.searchResultHandler?(weathers)
                }
            }
        }
    }
    
    internal func fetchSuccessfulQuery() {
        self.storage.fetchSuccessfulQuery { (array, error) in
            if error == nil {
                self._queryResult = array ?? Array<SingleQuery>()
                self.queryResultHandler?(self._queryResult)
            }
        }
    }

    unowned internal var outputs : SearchModelOutputs { return self }
    
    internal var noSearchResultHandler: ((String) -> Void)?
    internal var searchResultHandler: ((Array<WEATHER>) -> Void)?
    internal var queryResultHandler: ((Array<SingleQuery>) -> Void)?
    internal var requestWeatherDataPair: (request: REQUEST, weather: WEATHER) {
        return ( request : _lastRequest ?? REQUEST( with : NSDictionary()), weather : _lastWeather ?? WEATHER(with : NSDictionary()) )
    }
    
    
    fileprivate let storage : StorageType
    fileprivate let network : NetworkType
    
    init( storage : StorageType, network : NetworkType ) {
        self.storage = storage
        self.network = network
    }
    
    init() {
        self.storage = Storage(with: PersistentStorageDataSource.coredata )
        self.network = Network()
    }
    
    fileprivate var _queryResult : Array<SingleQuery> = Array<SingleQuery>()
    
    fileprivate var _lastRequest : REQUEST?
    fileprivate var _lastWeather : WEATHER?
}
