//
//  WeatherResultVM.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 6..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

protocol WeatherResultVMInputs : class {
    func reloadData()
    func initialLoad()
}

protocol WeatherResultVMOutputs : class {
    var weather : WeatherResultCellVMInOuts? { get }
    var viewNeedsUpdateHandler : (() -> Void)? { get set }
    var refreshFinishedHandler : (() -> Void)? { get set }
}

protocol WeatherResultVMInOuts : class {
    var inputs : WeatherResultVMInputs { get }
    var outputs : WeatherResultVMOutputs { get }
}

internal final class WeatherResultVM : WeatherResultVMInOuts, WeatherResultVMOutputs, WeatherResultVMInputs {

    unowned internal var inputs: WeatherResultVMInputs { return self }
    
    internal func reloadData() {
        model.inputs.reloadDataFromServer()
    }
    
    internal func initialLoad() {
        guard let newValue = model.outputs.weather else {
            return
        }
        _weather = WeatherResultCellsVM(with: newValue)
        DispatchQueue.main.async {
            self.viewNeedsUpdateHandler?()
        }
    }
    
    unowned internal var outputs: WeatherResultVMOutputs { return self }
    
    internal var weather: WeatherResultCellVMInOuts? {
        return _weather
    }
    internal var viewNeedsUpdateHandler: (() -> Void)?
    internal var refreshFinishedHandler: (() -> Void)?
    fileprivate var _isloading: Bool = false
    
    
    func bind() {
        model.outputs.weatherRequestDidFinishedLoadHandler = { [unowned self] () -> Void in
            guard let weather = self.model.outputs.weather else {
                return
            }
            self._weather = WeatherResultCellsVM(with: weather)
            
            DispatchQueue.main.async {
                self.viewNeedsUpdateHandler?()
                self.refreshFinishedHandler?()
            }
        }
    }
    
    fileprivate let model : WeatherResultModelInOuts
    fileprivate var _weather : WeatherResultCellVMInOuts?
    
    required init( with model : WeatherResultModelInOuts ) {
        self.model = model
        bind()
    }
}
