//
//  PersistentStorage.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 4..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit
import CoreData


public enum StorageFetchError : Error {
    case unknown
    case nodata
}

public enum CoreDataError : Error {
    case persistentStorage
}

private let _sharedCoreDataPresistentStorage = (try? CoreDataPersistentStorage.generateSqlitePersistentStoreCoordinator(name: "weather_woroc")) == nil ? nil : CoreDataPersistentStorage( persistentStorage : try! CoreDataPersistentStorage.generateSqlitePersistentStoreCoordinator(name: "weather_woroc") )

public protocol Fetchable {
    func fetchSuccesfulQuery( completion : @escaping (Array<NSObject>?) -> () ) throws
    func writeSuccesfulQuery( query : String ) throws
}

final public class CoreDataPersistentStorage {
    fileprivate let storageManagedObjectContext : NSManagedObjectContext
    fileprivate let mainManagedObjectContext : NSManagedObjectContext
    
    public static let shared = _sharedCoreDataPresistentStorage
    
    fileprivate static func generateManagedObjectModel( name : String ) throws -> NSManagedObjectModel {
        let modelUrl = Bundle.main.url(forResource: name, withExtension: "momd") ?? URL(string: "http://www.dummy.com")!
        guard let mom = NSManagedObjectModel(contentsOf: modelUrl) else {
            throw CoreDataError.persistentStorage
        }
        return mom
    }
    
    internal static func generateSqlitePersistentStoreCoordinator( name : String ) throws -> NSPersistentStoreCoordinator {
        guard let mom = try? CoreDataPersistentStorage.generateManagedObjectModel(name: name) else {
            throw CoreDataError.persistentStorage
        }
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docUrl = urls[urls.endIndex-1]
        let storeURL = docUrl.appendingPathComponent(name + ".sqlite")
        let _ = try? psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        
        return psc
    }
    
    internal static func generateMemoryPersistentStoreCoordinator( name : String ) throws -> NSPersistentStoreCoordinator {
        guard let mom = try? CoreDataPersistentStorage.generateManagedObjectModel(name: name) else {
            throw CoreDataError.persistentStorage
        }
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        let _ = try? psc.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
        return psc
    }
    
    init( persistentStorage psc : NSPersistentStoreCoordinator ) {
        storageManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        storageManagedObjectContext.persistentStoreCoordinator = psc
        
        mainManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        mainManagedObjectContext.parent = storageManagedObjectContext
    }
}

extension CoreDataPersistentStorage : Fetchable {
    public func fetchSuccesfulQuery( completion : @escaping (Array<NSObject>?) -> () ) throws {
        let fetched = self.fetch(entity: "SuccessfulQuery", sortBy: "date", ascending: false, fetchLimit: 10)
        completion( fetched )
    }
    
    public func writeSuccesfulQuery(query: String) throws  {
        let fetched = self.fetch(entity: "SuccessfulQuery", sortBy: "date", ascending: false, fetchLimit: 10)
        let querycount = fetched?.filter { (managedObject) -> Bool in
            return managedObject.value(forKey: "query" ) as? String == query as String
            }.count
        if querycount! > 0 {
            return
        }
        do {
            try insert { (moc) in
                if (fetched?.count)! >= 10 {
                    let first = fetched?.last
                    first?.setValue(query, forKey: "query")
                    first?.setValue(NSDate(), forKey: "date")
                } else {
                    let object = NSEntityDescription.insertNewObject(forEntityName: "SuccessfulQuery", into: moc) as? SuccessfulQuery
                    object?.date = NSDate()
                    object?.query = query == "" ? nil : query
                }
            }
        } catch {
            throw error
        }
        
    }
}

extension CoreDataPersistentStorage {
    internal func insert( context : @escaping (NSManagedObjectContext) -> () ) throws {
        let privateObjectManagedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateObjectManagedContext.parent = mainManagedObjectContext
        
        context( privateObjectManagedContext )
        let currentOmc : ()? = try? privateObjectManagedContext.save()
        let mainOmc : ()? = try? mainManagedObjectContext.save()
        let storageOmc : ()? = try? storageManagedObjectContext.save()

        if currentOmc == nil || mainOmc == nil || storageOmc == nil {
            throw CoreDataError.persistentStorage
        }
    }
    
    fileprivate func fetch( with request : NSFetchRequest<NSManagedObject> ) -> Array<NSManagedObject>? {
        let fetchedObjects = try? mainManagedObjectContext.fetch(request)
        return fetchedObjects
    }
    
    public func fetch( entity name : String ) -> Array<NSManagedObject>? {
        let request = NSFetchRequest<NSManagedObject>(entityName: name)
        return fetch(with: request)
    }
    
    public func fetch( entity name : String, sortBy sortDescriptor : String, ascending : Bool ) -> Array<NSManagedObject>? {
        
        let request = NSFetchRequest<NSManagedObject>(entityName: name)
        request.sortDescriptors = [NSSortDescriptor(key: sortDescriptor, ascending: ascending )]
        return fetch(with: request)
    }
    
    public func fetch( entity name : String, sortBy sortDescriptor : String, ascending : Bool, fetchLimit : Int ) -> Array<NSManagedObject>? {
        let request = NSFetchRequest<NSManagedObject>(entityName: name)
        request.sortDescriptors = [NSSortDescriptor(key: sortDescriptor, ascending: ascending )]
        request.fetchLimit = fetchLimit
        return fetch(with: request)
    }
}
