//
//  WeatherResultCellVM.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 6..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

protocol WeatherResultCellVMOutputs : class {
    var humidity : String? { get }
    var weatherIcon : UIImage? { get }
    var observationTime : String? { get }
    var weatherDescription : String? { get }
    var viewNeedsUpdateImageHandler : (() -> Void)? { get set }
}

protocol WeatherResultCellVMInOuts : class {
    var outputs : WeatherResultCellVMOutputs { get }
}

internal final class WeatherResultCellsVM : WeatherResultCellVMOutputs,  WeatherResultCellVMInOuts{
    unowned internal var outputs : WeatherResultCellVMOutputs { return self }
    
    internal var humidity : String? {
        return _humidity
    }
    internal var weatherIcon: UIImage? {
        return _weatherIcon
    }
    internal var observationTime: String? {
        return _observationTime
    }
    internal var weatherDescription: String? {
        return _weatherDescription
    }
    internal var viewNeedsUpdateImageHandler: (() -> Void)? {
        didSet {
            if _datamodel.weatherIcon == nil || _datamodel.weatherIcon!.url == nil {
                self._weatherIcon = nil
                DispatchQueue.main.async { [unowned self] in
                    self.viewNeedsUpdateImageHandler?()
                }
            } else {
                URLSession.shared.dataTask(with: _datamodel.weatherIcon!.url!.https ) { (data, response, error) in
                    guard let responseData = data else {
                        return
                    }
                    guard let image = UIImage(data: responseData) else {
                        return
                    }
                    self._weatherIcon = image
                    DispatchQueue.main.async { [unowned self] in
                        self.viewNeedsUpdateImageHandler?()
                    }
                    }.resume()
            }
        }
    }
    
    fileprivate var _humidity : String?
    fileprivate var _weatherIcon: UIImage?
    fileprivate var _observationTime: String?
    fileprivate var _weatherDescription: String?
    fileprivate var _datamodel : WEATHER
    
    init( with datamodel : WEATHER ) {
        _datamodel = datamodel
        
        _humidity = _datamodel.humidity
        _observationTime = _datamodel.observation_time
        _weatherDescription = _datamodel.weatherDesc
    }
}
