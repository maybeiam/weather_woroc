//
//  Storage.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 6..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

public protocol StorageRepresentationType {
    var datasource : Fetchable? { get }
}

public enum PersistentStorageDataSource : StorageRepresentationType {
    case coredata
    
    public var datasource : Fetchable? {
        switch self {
        case .coredata:
            return CoreDataPersistentStorage.shared
        }
    }
}

public protocol StorageType : class {
    var persistantStorage : Fetchable? { get }
    init( with storage : StorageRepresentationType? )
    
    func fetch<T:DataModelType>( query : Query, completion : @escaping (Array<T>?, StorageFetchError?) -> Void )
    func fetchSuccessfulQuery( completion : @escaping (Array<SingleQuery>?, StorageFetchError?) -> Void )
    func insertSuccessfulQuery( query : String )
}

final public class Storage : StorageType {
    public init(with storage: StorageRepresentationType? ) {
        persistantStorage = storage?.datasource
    }
    
    public var persistantStorage : Fetchable?
    
    public func fetch<T : DataModelType>(query: Query, completion: @escaping (Array<T>?, StorageFetchError?) -> Void) {
        switch query {
        case .search:
            do {
                try persistantStorage?.fetchSuccesfulQuery(completion: { (datas) in
                    let fatched = datas.map{ datas in datas.map{ data in T( with : data ) } }
                    completion( fatched, nil )
                })
            } catch {
                completion( nil, StorageFetchError.unknown )
            }
        }
    }
    
    public func fetchSuccessfulQuery(completion: @escaping (Array<SingleQuery>?, StorageFetchError?) -> Void) {
        self.fetch(query: .search) { (datas, error) in
            completion( datas, error )
        }
    }
    
    public func insertSuccessfulQuery(query: String) {
        do {
            try persistantStorage?.writeSuccesfulQuery(query: query)
        } catch {
            assert(false, "developer should know error")
            // log the error developer should know
        }
    }
}
