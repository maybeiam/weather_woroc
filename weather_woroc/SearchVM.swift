//
//  SearchVM.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 6..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

protocol SearchVMInputs : class {
    func searchButtonPressed( query : String )
    func didSelectSuggestion( at index : Int )
    func didEnterSearchTextField()
    func didLeaveSearchTextField()
}

protocol SearchVMOutputs : class {
    var suggestionListCellVM : Array<SearchCellVMInouts> { get }
    var suggestionListTableViewIsHidden : Bool { get }
    var searchRequestResult : ((WeatherResultVMInOuts) -> Void)? { get set }
    var searchRequestNodataResult : ((String) -> Void)? { get set }
    var viewNeedsUpdate : (() -> Void)? { get set }
}

protocol SearchVMInOuts : class {
    var inputs : SearchVMInputs { get }
    var outputs : SearchVMOutputs { get }
}

internal final class SearchVM: SearchVMInputs, SearchVMOutputs, SearchVMInOuts {
    
    private let model : SearchModelInOuts
    required init( with model : SearchModelInOuts ) {
        self.model = model
        bind()
    }
    
    unowned internal var inputs: SearchVMInputs { return self }
    internal func searchButtonPressed(query: String) {
        model.inputs.searchRequestReceived(query: query)
    }
    
    internal func didSelectSuggestion(at index: Int) {
        model.inputs.searchRequestReceived(query: _suggestionList[index] )
    }
    
    internal func didEnterSearchTextField() {
        model.inputs.fetchSuccessfulQuery()
        _suggestionListTableViewIsHidden = false
        
        DispatchQueue.main.async { [unowned self] in
            self.viewNeedsUpdate?()
        }
    }
    
    internal func didLeaveSearchTextField() {
        _suggestionListTableViewIsHidden = true
        
        DispatchQueue.main.async { [unowned self] in
            self.viewNeedsUpdate?()
        }
    }
    
    unowned internal var outputs: SearchVMOutputs { return self }
    
    internal var suggestionListTableViewIsHidden : Bool {
        return _suggestionListTableViewIsHidden
    }
    internal var suggestionListCellVM: Array<SearchCellVMInouts> {
        return _suggestionListCellVM
    }

    internal var searchRequestNodataResult: ((String) -> Void)?
    internal var searchRequestResult: ((WeatherResultVMInOuts) -> Void)?
    internal var viewNeedsUpdate: (() -> Void)?
    
    fileprivate var _suggestionList : Array<String> = Array<String>()
    fileprivate var _suggestionListCellVM : Array<SearchCellVMInouts> = Array<SearchCellVMInouts>()
    fileprivate var _suggestionListTableViewIsHidden : Bool = true
    fileprivate var _weatherResultVM : WeatherResultVMInOuts?
    
    func bind() {
        model.outputs.queryResultHandler = { [unowned self] (result) -> Void in
            self._suggestionList = result.filter{ $0.query != nil}.map{ (query) -> String in
                return query.query!
            }
            self._suggestionListCellVM = self._suggestionList.map{ string -> SearchCellVMInouts in
                return SearchCellVM(with: string)
            }
            
            DispatchQueue.main.async { [unowned self] in
                self.viewNeedsUpdate?()
            }
        }
        
        model.outputs.searchResultHandler = { [unowned self] (weathers) -> Void in
            let weatherResultModel = WeatherResultModel(request: self.model.outputs.requestWeatherDataPair.request, weather: self.model.outputs.requestWeatherDataPair.weather)
            self._weatherResultVM = WeatherResultVM(with: weatherResultModel )
            DispatchQueue.main.async { [unowned self] in
                self.viewNeedsUpdate?()
                self.searchRequestResult?( self._weatherResultVM! )
            }
        }
        model.outputs.noSearchResultHandler = { [unowned self] (string) -> Void in
            DispatchQueue.main.async { [unowned self] in
                self.searchRequestNodataResult?( string )
            }
        }

    }
}
