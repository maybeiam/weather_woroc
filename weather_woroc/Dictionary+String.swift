//
//  Dictionary+String.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 4..
//  Copyright © 2017년 Jin. All rights reserved.
//

extension Dictionary where Key : ExpressibleByStringLiteral, Value : ExpressibleByStringLiteral {
    public var query : String {
        return self.map { (key, value) -> String in
            return "\(key as! String)=\(value)"
        }.joined(separator: "&")
    }
}

