//
//  DataModel.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 5..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

public protocol DataModelType {
    init( with dictionary : NSDictionary )
    init( with nsobject : NSObject )
}

public protocol ListType {
    static func array( from dictionary : NSDictionary ) -> Array<Self>
}
