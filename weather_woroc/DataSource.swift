//
//  DataSource.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

import UIKit

protocol WeatherTableViewDataSource : UITableViewDataSource {
    func configureCell( cell : UITableViewCell, at indexPath : IndexPath )
}
