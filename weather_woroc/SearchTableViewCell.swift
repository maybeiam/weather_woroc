//
//  SearchTableViewCell.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

import UIKit

internal final class SearchTableViewCell: UITableViewCell {

    internal static var reuseIdentifier: String = "SearchTableViewCellReuseIdentifier"
    
    weak internal var viewmodel : SearchCellVMInouts? {
        didSet {
            bindViewModel()
        }
    }
    
    internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        queryLabel.text = viewmodel.outputs.query
    }
    
    @IBOutlet weak var queryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
