//
//  SINGLEQUERY.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 5..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

public struct SingleQuery : DataModelType {
    var query : String?
    public init(with dictionary: NSDictionary) {
        query = dictionary.object(forKey: "query") as? String ?? ""
    }
    
    public init(with nsobject: NSObject) {
        query = nsobject.value(forKey: "query") as? String ?? ""
    }
}
