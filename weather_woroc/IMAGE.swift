//
//  IMAGE.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 5..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

public struct IMAGE : DataModelType {
    public var image : UIImage?
    public var url : URL?
    
    public init(with nsobject: NSObject) {
        url = nsobject.value(forKey: "url") as? URL
    }
    
    public init(with dictionary: NSDictionary) {
        url = ((dictionary.object(forKey: "value") as? String) ?? "").url
    }
}
