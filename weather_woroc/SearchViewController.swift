//
//  SearchViewController.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

import UIKit

internal final class SearchViewController: UIViewController {
    
    fileprivate let datasource = SuccessfulQueryDataSource()
    fileprivate var delegates : SearchViewControllerDelegates?
    
    internal static func instantiate() -> SearchViewController {
        let viewcontroller = Storyboard.Main.instantiate(SearchViewController.self)
        
        let model = SearchModel()
        let viewmodel = SearchVM(with: model)
        viewcontroller.viewmodel = viewmodel
        
        return viewcontroller
    }
    
    internal var viewmodel : SearchVMInOuts?
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var mainTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegates = SearchViewControllerDelegates( responsible : viewmodel )
        
        mainTableView.dataSource = datasource
        mainTableView.delegate = delegates
        searchTextField.delegate = delegates
        bindViewModel()
        // Do any additional setup after loading the view.
    }
    
    internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.outputs.viewNeedsUpdate = { [unowned self] () -> Void in
            self.datasource.load(value: viewmodel.outputs.suggestionListCellVM, at: 0)
            self.mainTableView.isHidden = viewmodel.outputs.suggestionListTableViewIsHidden
            self.mainTableView.reloadData()
        }
        
        viewmodel.outputs.searchRequestResult = { [unowned self] (viewmodel) -> Void in
            let viewController = WeatherResultViewController.instantiate(with: viewmodel)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        viewmodel.outputs.searchRequestNodataResult = { [unowned self] (msg) -> Void in
            let alertController = UIAlertController(title: "No data message", message: msg, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.inputs.searchButtonPressed(query: searchTextField.text ?? "" )
    }
}
