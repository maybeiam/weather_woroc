//
//  SearchCellVM.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 6..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

protocol SearchCellVMOutputs : class {
    var query : String { get }
}

protocol SearchCellVMInouts : class {
    var outputs : SearchCellVMOutputs { get }
}

internal final class SearchCellVM : SearchCellVMInouts, SearchCellVMOutputs {
    
    init( with string : String ) {
        _query = string
    }
    
    unowned internal var outputs : SearchCellVMOutputs { return self }
    internal var query: String {
        return _query
    }
    fileprivate var _query : String = ""
}

