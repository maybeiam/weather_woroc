//
//  IconCell.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//
import UIKit

internal final class IconCell: WeatherResultTableViewCell {
    
    internal static var reuseIdentifier: String = "IconCellReuseIdentifier"
    
    override internal func bindViewModel() {
        
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.outputs.viewNeedsUpdateImageHandler = { () -> Void in
            self.iconImageView.image = viewmodel.outputs.weatherIcon
        }
    }
    
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
