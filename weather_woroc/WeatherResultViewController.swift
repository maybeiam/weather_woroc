//
//  WeatherResultViewController.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

import UIKit

internal final class WeatherResultViewController: UIViewController {
    
    fileprivate let datasource = WeatherResultsDataSource()
    internal var viewmodel : WeatherResultVMInOuts?
    internal static func instantiate( with viewmodel : WeatherResultVMInOuts ) -> WeatherResultViewController {
        let viewcontroller = Storyboard.Weather.instantiate(WeatherResultViewController.self)
        viewcontroller.viewmodel = viewmodel
        
        return viewcontroller
    }
    @IBOutlet weak var mainTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViewModel()
        
        mainTableView.dataSource = datasource
        mainTableView.rowHeight = 80.0
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector( reload ), for: .valueChanged )
        mainTableView.refreshControl = refreshControl
        
        guard let viewmodel = viewmodel else {
            return
        }
        
        viewmodel.inputs.initialLoad()
        // Do any additional setup after loading the view.
    }
    
    internal func reload() {
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.inputs.reloadData()
    }
    
    internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        viewmodel.outputs.refreshFinishedHandler = { [unowned self] () -> Void in
            self.mainTableView.refreshControl?.endRefreshing()
        }
        viewmodel.outputs.viewNeedsUpdateHandler = { [unowned self] () -> Void in
            self.datasource.load(value: [viewmodel.outputs.weather as Any], at: 0)
            self.mainTableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
