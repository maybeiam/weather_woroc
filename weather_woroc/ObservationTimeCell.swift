//
//  ObservationTimeCell.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

import UIKit

internal final class ObservationTimeCell: WeatherResultTableViewCell {
    
    internal static var reuseIdentifier: String = "ObservationTimeCellReuseIdentifier"
    
    override internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        observationTimeLabel.text = viewmodel.outputs.observationTime
    }
    
    @IBOutlet weak var observationTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
