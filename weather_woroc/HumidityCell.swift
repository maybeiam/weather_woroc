//
//  HumidityCell.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

import UIKit

internal final class HumidityCell: WeatherResultTableViewCell {
    
    internal static var reuseIdentifier: String = "HumidityCellReuseIdentifier"
    override internal var viewmodel: WeatherResultCellVMInOuts? {
        didSet {
            bindViewModel()
        }
    }
    
    override internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        humidityLabel.text = viewmodel.outputs.humidity
    }
    
    
    @IBOutlet weak var humidityLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
    }
    
}
