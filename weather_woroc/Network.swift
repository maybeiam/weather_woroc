//
//  Network.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 4..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

public enum NetworkError : Error {
    case unknown
    case wrongurl
    case nodata
}

public protocol NetworkType {
    func request( api : APISType, completion : @escaping (NSDictionary?, NetworkError?) -> () )
}

public struct Network : NetworkType {
    
    fileprivate static let parser = JSONParser()
    fileprivate static let errorparser = WeatherErrorParser()
    
    public func request(api: APISType, completion : @escaping (NSDictionary?, NetworkError?) -> ()) {
        guard let urlRequest = api.request else {
            completion( nil, .wrongurl )
            return
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: urlRequest) {  data, response, error in
            let dictionary = Network.parser.parse(with: data)
            let dataerror = Network.errorparser.parse(with: dictionary)
            if dataerror == nil {
                completion( dictionary!, nil )
            } else {
                completion( dataerror!, nil )
            }
        }
        task.resume()
    }
}
