//
//  SuccessfulQueryDataSource.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

import UIKit

internal final class SuccessfulQueryDataSource: NSObject, WeatherTableViewDataSource {
    fileprivate var datasource : Dictionary<Int, Array<Any>> = Dictionary<Int, Array<Any>>()
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return datasource.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource[section]?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.reuseIdentifier, for: indexPath)
        configureCell(cell: cell, at: indexPath)
        return cell
    }
    
    internal func load( value : Array<Any>, at section : Int ) {
        datasource[section] = value
    }
    
    internal func configureCell(cell: UITableViewCell, at indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            let currentCell = cell as! SearchTableViewCell
            guard let currentSection = datasource[indexPath.section] else {
                return
            }
            let viewmodel = currentSection[indexPath.row] as! SearchCellVMInouts
            currentCell.viewmodel = viewmodel
        default:
            return
        }
    }
}
