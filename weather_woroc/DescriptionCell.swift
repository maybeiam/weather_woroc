//
//  DescriptionCell.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

import UIKit

internal final class DescriptionCell: WeatherResultTableViewCell {
    
    internal static var reuseIdentifier: String = "DescriptionCellReuseIdentifier"
    
    override internal func bindViewModel() {
        guard let viewmodel = viewmodel else {
            return
        }
        descriptionLabel.text = viewmodel.outputs.weatherDescription
    }
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
