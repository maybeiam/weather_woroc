//
//  SearchViewControllerDelegates.swift
//  weather_woroc
//
//  Created by 1002220 on 07/02/2017.
//  Copyright © 2017 Jin. All rights reserved.
//

import UIKit

internal final class SearchViewControllerDelegates: NSObject {
    fileprivate var _viewmodel : SearchVMInOuts?
    required init( responsible vm : SearchVMInOuts? ) {
        _viewmodel = vm
    }
}

extension SearchViewControllerDelegates : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewmodel = _viewmodel else {
            return
        }
        viewmodel.inputs.didSelectSuggestion(at: indexPath.row)
    }
}

extension SearchViewControllerDelegates : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let viewmodel = _viewmodel else {
            return
        }
        viewmodel.inputs.didLeaveSearchTextField()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let viewmodel = _viewmodel else {
            return
        }
        viewmodel.inputs.didEnterSearchTextField()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let viewmodel = _viewmodel else {
            return true
        }
        textField.resignFirstResponder()
        viewmodel.inputs.searchButtonPressed(query: textField.text ?? "" )
        return true
    }
}
