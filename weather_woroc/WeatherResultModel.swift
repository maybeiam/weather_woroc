//
//  WeatherResultModel.swift
//  weather_woroc
//
//  Created by Shepherd on 2017. 2. 7..
//  Copyright © 2017년 Jin. All rights reserved.
//

import UIKit

protocol WeatherResultModelInputs : class {
    func reloadDataFromServer()
}

protocol WeatherResultModelOutputs : class {
    var weather : WEATHER? { get }
    var weatherRequestDidFinishedLoadHandler : (() -> Void)? { get set }
}

protocol WeatherResultModelInOuts : class {
    var inputs : WeatherResultModelInputs { get }
    var outputs : WeatherResultModelOutputs { get }
}

internal final class WeatherResultModel: WeatherResultModelInputs, WeatherResultModelOutputs, WeatherResultModelInOuts {
    
    unowned internal var inputs: WeatherResultModelInputs { return self }

    internal func reloadDataFromServer() {
        guard let request = _request else {
            self.weatherRequestDidFinishedLoadHandler?()
            return
        }
        guard let query = request.query else {
            self.weatherRequestDidFinishedLoadHandler?()
            return
        }
        if query.characters.count <= 0 {
            self.weatherRequestDidFinishedLoadHandler?()
            return
        }
        
        network.request(api: APIs.search(query: query)) { [unowned self] (result, error) in
            if error == nil {
                let dictionary = result
                let weathers = WEATHER.array(from: dictionary ?? NSDictionary() )
                if weathers.count > 0 {
                    self._weather = weathers.last!
                }
                let request = REQUEST.array(from: dictionary ?? NSDictionary() )
                if request.count > 0 {
                    self._request = request.last!
                }
                self.weatherRequestDidFinishedLoadHandler?()
            }
        }
    }
    
    unowned internal var outputs: WeatherResultModelOutputs { return self }
    internal var weather: WEATHER? {
        return _weather
    }
    internal var weatherRequestDidFinishedLoadHandler: (() -> Void)?
    
    fileprivate var _weather : WEATHER?
    fileprivate var _request : REQUEST?
    fileprivate let network : NetworkType
    
    init( network : NetworkType ) {
        self.network = network
    }
    
    init() {
        self.network = Network()
    }
    
    required init( request : REQUEST, weather : WEATHER ) {
        self.network = Network()
        _request = request
        _weather = weather
    }
    
    init( request : REQUEST, weather : WEATHER, network : NetworkType ) {
        self.network = network
        _request = request
        _weather = weather
    }

}
